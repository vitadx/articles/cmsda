# Contrastive Multi-Source Domain Adaptation (CMSDA)


<center>
	<h2>| <a href="https://arxiv.org/abs/2106.16093">Paper</a> |</h2>
</center>

This repository contains the official pytorch implementation of the method Contrastive Multi-Source Domain Adaptation (CMSDA) presented in the paper [Multi-Source Domain Adaptation via supervised contrastive learning and confident consistency regularization](https://arxiv.org/abs/2106.16093).

<img src="./imgs/CMSDA.png" width="100%" align="center" alt="Diagram of the CMSDA framework">


CMSDA contains 3 shared components: a features extractor <img src="https://i.upmath.me/svg/F" alt="F" />, a projection head <img src="https://i.upmath.me/svg/G" alt="G" /> and a classification head <img src="https://i.upmath.me/svg/C" alt="C" />. Source examples are strongly augmented via <img src="https://i.upmath.me/svg/t_s(.)" alt="t_s(.)" /> and interpolated using MixUp. Target examples are weakly and strongly augmented via <img src="https://i.upmath.me/svg/t_w(.)" alt="t_w(.)" /> and  <img src="https://i.upmath.me/svg/t_s(.)" alt="t_s(.)" />. All examples are fed to <img src="https://i.upmath.me/svg/F" alt="F" /> to produce the embeddings <img src="https://i.upmath.me/svg/%5Cbm%7Bh%7D" alt="\bm{h}" />. In the top branch, source embeddings <img src="https://i.upmath.me/svg/%5Cbm%7Bh%7D" alt="\bm{h}" /> are given to <img src="https://i.upmath.me/svg/G" alt="G" /> to produce the representations <img src="https://i.upmath.me/svg/%5Cbm%7B%5Ctilde%7Bz%7D%7D" alt="\bm{\tilde{z}}" /> which are used to align source class conditional distributions via minimization of the interpolated supervised contrastive loss <img src="https://i.upmath.me/svg/%5Cmathcal%7BL%7D_%7BISCL%7D" alt="\mathcal{L}_{ISCL}" />. In the bottom branch, all embeddings <img src="https://i.upmath.me/svg/%5Cbm%7Bh%7D" alt="\bm{h}" /> are fed to <img src="https://i.upmath.me/svg/C" alt="C" /> to produce probability vectors <img src="https://i.upmath.me/svg/%5Cbm%7B%5Chat%7By%7D%7D" alt="\bm{\hat{y}}" />, <img src="https://i.upmath.me/svg/%5Cbm%7B%5Chat%7By%7D%5E%7B(s)%7D%7D" alt="\bm{\hat{y}^{(s)}}" /> and <img src="https://i.upmath.me/svg/%5Cbm%7B%5Chat%7By%7D%5E%7B(w)%7D%7D" alt="\bm{\hat{y}^{(w)}}" />. These probability vectors are used to learn discriminative features via minimization of cross entropy loss <img src="https://i.upmath.me/svg/%5Cmathcal%7BL%7D_%7Bce%7D" alt="\mathcal{L}_{ce}" /> and FixMatch loss <img src="https://i.upmath.me/svg/%5Cmathcal%7BL%7D_%7Bunsup%7D" alt="\mathcal{L}_{unsup}" />.

## Requirements

### Python Dependencies

<ul>
	<li>python 3.7</li>
	<li>pytorch</li>
	<li>torchvision</li>
	<li>cudatoolkit 10.2</li>
	<li>tensorboard</li>
	<li>pandas</li>
	<li>tabulate</li>
	<li>tqdm</li>
</ul>

### Setup environment via Conda

If you are using conda to manage your python environments, you need first to create a conda environment and install the different dependencies via the following command:
```bash
conda create -n cmsda -c pytorch python=3.7 \
pytorch \
torchvision \
cudatoolkit=10.2 \
tensorboard \
pandas \
tabulate \
tqdm
```
and then activate the environment:
```bash
conda activate cmsda
```

## Download data

To run experiments on a dataset, you first need to download the dataset and create the `json` file that contains the different train/test splits for the different domains. The json file must be structured like the json file example for MiniDomainNet provided <a href="./data/mini_domain_net.json" target="_blank">here</a>. For simplicity and to be easier to read, the provided json contains only few examples with a single class.

The different datasets used in our paper can be downloaded here:

| Dataset       | Link                                |
|:--------------|:------------------------------------|
| DomainNet     | <a href="http://ai.bu.edu/M3SDA/#dataset" target="_blank">data</a> |
| MiniDomainNet | <a href="https://drive.google.com/file/d/15rrLDCrzyi6ZY-1vJar3u7plgLe4COL7/view" target="_blank">data</a> |
| PACS          | <a href="https://drive.google.com/file/d/1m4X4fROCCXMO0lRLrr6Zz9Vb3974NWhE/view" target="_blank">data</a> |

## Train the model

The dataset, the different hyperparameters of the framework and the directories of the logs/checkpoints/results must be specified in a config file located in the directory `configs`.
To write you own config file, you can copy one of the config files already available in the directory `configs` such as `domain_net.py`, `mini_domain_net.py` or  `office_home.py`.
</br> 
To train and evaluate the model on every (source, target) possible combinations of a dataset, make sure to import the config file at the beginning of `train.py` and run the script:
```bash
python train.py
```

## References

If you use our work for further research, please cite us:
```
@article{scalbert2021multi,
  title={Multi-Source domain adaptation via supervised contrastive learning and confident consistency regularization},
  author={Scalbert, Marin and Vakalopoulou, Maria and Couzini{\'e}-Devy, Florent},
  journal={arXiv preprint arXiv:2106.16093},
  year={2021}
}
```

## Contact us

If you have any technical issues with the code or questions regarding the paper, you can contact us via this [mail adress](mailto:marin.scalbert@centralesupelec.fr).