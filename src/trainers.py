
import torch
import tqdm
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.tensorboard import SummaryWriter

from augmentations import MixUp
from metrics import AverageMeter
from losses import (
    InterpolatedSupervisedContrastiveLoss,
    SoftmaxCrossEntropyLoss)
from evaluators import Evaluator


class Trainer():

    def __init__(self):
        pass

    def create_metrics(self):
        metrics = {}
        for split in ["train"]:
            metrics[split] = {
                "loss": AverageMeter(name="loss"),
                "ce_loss": AverageMeter(name="ce_loss"),
                "iscl_loss": AverageMeter(name="iscl_loss"),
                "fixmatch_loss": AverageMeter(name="fixmatch_loss"),
                "acc": AverageMeter(name="acc"),
                "kept_target_acc": AverageMeter(name="kept_target_acc"),
            }
        return metrics

    def train(
            self,
            cmsda,
            cmsda_ema,
            dataloaders,
            optimizer,
            scheduler,
            epochs,
            temperature,
            prob_threshold_fixmatch,
            lambda_unsup,
            lambda_iscl,
            mixup_alpha,
            device,
            use_mixed_precision,
            steps_per_epoch,
            log_dir):

        # Move model to appropriate device
        cmsda = cmsda.to(device)
        cmsda_ema = cmsda_ema.to(device)
        cmsda_ema = cmsda_ema.eval()

        # Create tensorboard logger
        writer = SummaryWriter(log_dir=log_dir)

        # Create metrics to log
        metrics = self.create_metrics()

        # Training loop
        grad_scaler = torch.cuda.amp.GradScaler(enabled=use_mixed_precision)

        # Create evaluator to evaluate after each epoch end
        evaluator = Evaluator()

        global_step = 0
        for epoch in range(epochs):

            # train mode
            cmsda = cmsda.train()

            # Renitialize metrics at each epoch beginning
            for split_name in metrics:
                for metric_name in metrics[split_name]:
                    metrics[split_name][metric_name].reset()

            pbar = tqdm.tqdm(dataloaders["source"]
                             ["train"], total=steps_per_epoch)
            for batch_index, (X_source_1, X_source_2, labels_source, domains_source) in enumerate(pbar):
                # remove the batch dimension added by dataloader as batch are
                # created in an IterableDataset
                X_source_1, X_source_2, labels_source, domains_source = X_source_1.squeeze(), \
                    X_source_2.squeeze(), labels_source.squeeze(), domains_source.squeeze(),
                X_source = torch.cat([X_source_1, X_source_2]).to(device)
                labels_source = labels_source.repeat(2).to(device)
                domains_source = domains_source.repeat(2).to(device)

                # Sample examples from target domains
                X_target_weak, X_target_strong, gt_labels_target, _ = next(
                    dataloaders["target"]["train"])
                X_target_weak = X_target_weak.to(device)
                X_target_strong = X_target_strong.to(device)
                gt_labels_target = gt_labels_target.to(device)

                self.train_on_batch(
                    cmsda=cmsda,
                    cmsda_ema=cmsda_ema,
                    X_source=X_source,
                    labels_source=labels_source,
                    domains_source=domains_source,
                    X_target_weak=X_target_weak,
                    X_target_strong=X_target_strong,
                    gt_labels_target=gt_labels_target,
                    temperature=temperature,
                    prob_threshold_fixmatch=prob_threshold_fixmatch,
                    lambda_unsup=lambda_unsup,
                    lambda_iscl=lambda_iscl,
                    optimizer=optimizer,
                    scheduler=scheduler,
                    use_mixed_precision=use_mixed_precision,
                    grad_scaler=grad_scaler,
                    mixup_alpha=mixup_alpha,
                    metrics=metrics,
                    writer=writer,
                    global_step=global_step,)
                global_step += 1

                pbar.set_description(
                    "Epochs: {} / {}, lr: {:.6f} --".format(
                        epoch+1, epochs, scheduler.get_last_lr()[0]) +
                    ", ".join(["{}: {:.3f}".format(metric.name, metric.avg)
                               for metric in metrics["train"].values()]))

                if (batch_index+1) == steps_per_epoch:
                    break

            # Set eval model for evaluation
            cmsda = cmsda.eval()

            # Log the metrics in tensorboard
            for metric_name in metrics["train"]:
                metric = metrics["train"][metric_name]
                writer.add_scalar(
                    "{}/{}".format("train", metric_name), metric.avg,
                    global_step=global_step)

            # Write the learning rate, losses and acc
            writer.add_scalar(
                "train/lr", scheduler.get_last_lr()[0],
                global_step=global_step)

            # Update the learning rate
            scheduler.step()

            if (epoch+1) % 20 == 0:
                print("Source test: ")
                source_perfs_per_domains = evaluator.evaluate(
                    cmsda=cmsda_ema,
                    dataloader=dataloaders["source"]["test"],
                    use_mixed_precision=use_mixed_precision,
                    device=device,
                    display_results=False)
                for domain, perf in source_perfs_per_domains.items():
                    tag = domain if domain != "all" else "source/all"
                    writer.add_scalar(
                        "test/acc/{}".format(tag),
                        perf,
                        global_step)

                print("Target test: ")
                target_perfs_per_domains = evaluator.evaluate(
                    cmsda=cmsda_ema,
                    dataloader=dataloaders["target"]["test"],
                    use_mixed_precision=use_mixed_precision,
                    device=device,
                    display_results=False)
                for domain, perf in target_perfs_per_domains.items():
                    tag = domain if domain != "all" else "target/all"
                    writer.add_scalar(
                        "test/acc/{}".format(tag),
                        perf,
                        global_step)
        writer.close()

    def train_on_batch(
            self,
            cmsda,
            cmsda_ema,
            X_source,
            labels_source,
            domains_source,
            X_target_weak,
            X_target_strong,
            gt_labels_target,
            temperature,
            prob_threshold_fixmatch,
            lambda_unsup,
            lambda_iscl,
            optimizer,
            scheduler,
            use_mixed_precision,
            grad_scaler,
            mixup_alpha,
            metrics,
            writer,
            global_step):

        # Zero gradients to avoid gradients accumulation
        optimizer.zero_grad()

        # Obtain target pseudo labels
        with torch.cuda.amp.autocast(enabled=use_mixed_precision):
            with torch.no_grad():
                cmsda_ema(X_target_weak, compute_z=False)
                _, logits_target_weak = cmsda_ema(
                    X_target_weak, compute_z=False)
                max_probs, target_pseudo_labels = torch.max(
                    logits_target_weak.softmax(dim=-1), dim=-1)
                confident_target_examples = max_probs.ge(
                    prob_threshold_fixmatch)
                n_confident_target_examples = confident_target_examples.sum().item()

        # Differentiate between mixed precision training and normal training
        with torch.cuda.amp.autocast(enabled=use_mixed_precision):

            X_source_mixed, y_source_1, y_source_2, lambdas = MixUp(
                alpha=mixup_alpha)(X_source, labels_source)
            z_source_mixed, logits_source_mixed = cmsda(
                X_source_mixed, compute_z=True)
            z_source_mixed = F.normalize(z_source_mixed, p=2, dim=1)
            _, logits_target_strong = cmsda(X_target_strong, compute_z=False)

            # Computes the different losses
            # 1) Interpolated cross entropy
            K = logits_source_mixed.size(-1)
            soft_targets = lambdas.unsqueeze(1) * F.one_hot(y_source_1, K) +\
                (1-lambdas).unsqueeze(1) * F.one_hot(y_source_2, K)
            ce_loss = SoftmaxCrossEntropyLoss()(
                inputs=logits_source_mixed.softmax(dim=-1),
                targets=soft_targets)

            # 2) Fixmatch loss
            if lambda_unsup:
                fixmatch_losses = F.cross_entropy(
                    input=logits_target_strong,
                    target=target_pseudo_labels,
                    reduction="none")
                fixmatch_loss = lambda_unsup * (
                    fixmatch_losses * confident_target_examples).mean()
            else:
                fixmatch_loss = 0

            # 3) Supervised contrastive loss on source
            iscl_loss = InterpolatedSupervisedContrastiveLoss(
                temperature=temperature)(
                    features=z_source_mixed,
                    labels_1=y_source_1,
                    labels_2=y_source_2,
                    dominant_labels=y_source_1,
                    lambdas=lambdas,
                    normalize=False) if lambda_iscl else 0
            iscl_loss = lambda_iscl * iscl_loss

            # Total loss
            loss = ce_loss + iscl_loss + fixmatch_loss

        # Compute gradients and update weights
        grad_scaler.scale(loss).backward()
        grad_scaler.step(optimizer)
        grad_scaler.update()

        cmsda_ema.module.update_ema_weights(cmsda.module)

        # Compute accuracies
        cmsda = cmsda.eval()
        with torch.no_grad():
            with torch.cuda.amp.autocast(enabled=use_mixed_precision):
                # source
                source_pred_labels = logits_source_mixed.argmax(dim=-1)
                source_acc = lambdas * source_pred_labels.eq(y_source_1) + \
                    (1-lambdas) * source_pred_labels.eq(y_source_2)
                source_acc = source_acc.mean()

                # kept target examples
                num_kept_target_examples = n_confident_target_examples
                kept_target_acc = (target_pseudo_labels[confident_target_examples]
                                   == gt_labels_target[confident_target_examples]).float().mean()

                # Update the averaged metrics
                metrics["train"]["loss"].update(
                    loss.item(), X_source_mixed.size(0))
                metrics["train"]["ce_loss"].update(
                    ce_loss.item(), labels_source.size(0))
                metrics["train"]["iscl_loss"].update(
                    iscl_loss.item() if lambda_iscl else 0, labels_source.size(0))
                metrics["train"]["fixmatch_loss"].update(
                    fixmatch_loss.item() if lambda_unsup else 0, logits_target_weak.size(0))
                metrics["train"]["acc"].update(
                    source_acc.item(), source_pred_labels.size(0))
                if num_kept_target_examples:
                    metrics["train"]["kept_target_acc"].update(
                        kept_target_acc.item(), num_kept_target_examples)
        cmsda = cmsda.train()
