import torch
from torch import nn
from torch.nn import functional as F


class InterpolatedSupervisedContrastiveLoss(nn.Module):

    def __init__(self, temperature):
        super(InterpolatedSupervisedContrastiveLoss, self).__init__()
        self.temperature = temperature

    def forward(
            self,
            features,
            labels_1,
            labels_2,
            dominant_labels,
            lambdas,
            normalize=True):
        """Computes the Interpolated Supervised Contrastive Loss (ISCL).

        Args:
            features (torch.Tensor): Features representation with shape (N ,d)
                N corresponds to batch size and d to features dimension.
            labels_1 (torch.Tensor): Grountruth labels associated to
                interpolation coefficients $\lambda$ with shape (N,)
            labels_2 (torch.Tensor): Grountruth labels associated to
                interpolation coefficients $1 - \lambda$ with shape (N,)
            dominant_labels (torch.Tensor): Dominant grountruth labels with
                shape (N,). dominant labels are the labels associated to the
                highest corresponding interpolation coefficients.
                (i. e $\max(\lambda, 1 - \lambda)$)
            lambdas (torch.Tensor): Interpolation coefficients drawn in Mixup
                with shape (N, ).
            normalize (bool, optional): Boolean to indicate whether or not
                apply a L2 normalization. If set to false, it means that
                `features`have already been normalized.

        Returns:
            Torch.Tensor: A loss scalar.
        """

        # Normalize features if required
        if normalize:
            features = F.normalize(features, dim=-1, p=2)
        # Get the pairwise distances matrix with shape (batch_size, batch_size)
        pairwise_distance = torch.matmul(
            features, features.T) / self.temperature
        # For numerical stability
        logits_max, _ = torch.max(pairwise_distance, dim=1, keepdim=True)
        pairwise_distance = pairwise_distance - logits_max.detach()
        # Compute losses for each sample
        losses = lambdas * self.compute_per_sample_loss(
            pairwise_distance, labels_1, dominant_labels) + \
            (1-lambdas) * self.compute_per_sample_loss(
                pairwise_distance, labels_2, dominant_labels)
        return losses.mean()

    def compute_per_sample_loss(
            self,
            pairwise_distance,
            query_labels,
            anchors_labels):

        n = pairwise_distance.size(0)
        anchor_matrix = torch.logical_not(
            torch.eye(n, dtype=torch.bool, device=pairwise_distance.device))
        # Get the positive matrix where row with index `i`
        # indicates for a query at index i which anchors have the labels y_i
        pos_matrix = query_labels.unsqueeze(1).eq(anchors_labels.unsqueeze(0))
        # Remove comparison between query and itself (i.e elements on the diagonal)
        pos_matrix = torch.logical_and(pos_matrix, anchor_matrix)
        # Get positive counts for each query
        pos_counts = (pos_matrix.sum(dim=-1)).type(pairwise_distance.type())

        exp_logits = torch.exp(pairwise_distance)
        losses = - (1 / pos_counts) * (pos_matrix * (
            pairwise_distance - torch.log(
                (anchor_matrix * exp_logits).sum(dim=-1, keepdim=True)
            ))).sum(dim=-1)
        return losses


class SoftmaxCrossEntropyLoss(nn.Module):

    def __init__(self):
        super(SoftmaxCrossEntropyLoss, self).__init__()

    def forward(self, inputs, targets):
        inputs = inputs.clamp(1e-6, 1)
        loss = - (targets * torch.log(inputs)).sum(dim=-1).mean()
        return loss
