import numpy as np
from PIL import Image

import torch
from torch.utils.data import Dataset, IterableDataset


class ClassificationDataset(Dataset):

    def __init__(
            self,
            img_paths,
            labels,
            domains,
            transform,
            index2label,
            index2domain):
        super(ClassificationDataset, self).__init__()
        self.img_paths = img_paths
        self.labels = labels
        self.domains = domains
        self.transform = transform
        self.index2label = index2label
        self.index2domain = index2domain

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, index):
        img_path = self.img_paths[index]
        pil_img = Image.open(img_path)
        img = self.transform(pil_img)
        pil_img.close()
        label = self.labels[index]
        domain = self.domains[index]
        return img, label, domain


class TwoStreamsDataset(Dataset):

    def __init__(
            self,
            img_paths,
            labels,
            domains,
            weak_transform,
            strong_transform,
            index2label,
            index2domain):
        super(TwoStreamsDataset, self).__init__()
        self.img_paths = img_paths
        self.labels = labels
        self.domains = domains
        self.weak_transform = weak_transform
        self.strong_transform = strong_transform
        self.index2label = index2label
        self.index2domain = index2domain

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, index):
        img = Image.open(self.img_paths[index])
        label = self.labels[index]
        domain = self.domains[index]
        weak_img = self.weak_transform(img)
        strong_img = self.strong_transform(img)
        img.close()
        return weak_img, strong_img, label, domain


class BalancedClassDomainTwoStreamDataset(IterableDataset):

    def __init__(
            self,
            img_paths,
            labels,
            domains,
            batch_size,
            sampled_examples_per_class_per_domain,
            transform,
            index2label,
            index2domain,
            shuffle):
        self.img_paths = img_paths
        self.labels = labels
        self.domains = domains
        self.batch_size = batch_size
        self.sampled_examples_per_class_per_domain = sampled_examples_per_class_per_domain
        self.transform = transform
        self.index2label = index2label
        self.index2domain = index2domain
        self.unique_labels = np.unique(self.labels)
        self.unique_domains = np.unique(self.domains)
        self.examples_per_class = self.sampled_examples_per_class_per_domain *\
            len(self.unique_domains)

        self.imgs_dict = {}
        for img_path, label, domain in zip(
                self.img_paths, self.labels, self.domains):
            if label not in self.imgs_dict:
                self.imgs_dict[label] = {}
            if domain not in self.imgs_dict[label]:
                self.imgs_dict[label][domain] = []
            self.imgs_dict[label][domain].append(img_path)

        for label in self.imgs_dict:
            for domain in self.imgs_dict[label]:
                self.imgs_dict[label][domain] = np.array(
                    self.imgs_dict[label][domain])

        self.num_classes_to_sampled = min(
            np.ceil(self.batch_size/self.examples_per_class).astype(int),
            len(self.unique_labels))

        if self.shuffle:
            print("Shuffling data...")
            indexes = np.arange(len(self.img_paths))
            np.random.shuffle(indexes)
            self.img_paths = [self.img_paths[index] for index in indexes]
            self.labels = [self.labels[index] for index in indexes]
            self.domains = [self.domains[index] for index in indexes]

    def __iter__(self):
        return self.balanced_domain_class_batch()

    def balanced_domain_class_batch(self):

        while True:

            sampled_classes = self.unique_labels[torch.randperm(
                len(self.unique_labels)).numpy()[:self.num_classes_to_sampled]]
            num_examples_per_class_per_domain = np.ceil(
                self.batch_size/(sampled_classes.size*len(
                    self.unique_domains))).astype(int)

            imgs_paths, imgs_1, imgs_2, labels, domains = [], [], [], [], []
            for sampled_class in sampled_classes:
                for domain in self.imgs_dict[sampled_class]:
                    img_paths = self.imgs_dict[sampled_class][domain]
                    sampled_indexes = torch.randint(
                        low=0,
                        high=len(img_paths),
                        size=(num_examples_per_class_per_domain,))
                    sampled_img_paths = img_paths[sampled_indexes.numpy()]
                    imgs_paths.extend(sampled_img_paths.tolist())
                    labels.extend([sampled_class] * len(sampled_img_paths))
                    domains.extend([domain] * len(sampled_img_paths))

            for img_path in imgs_paths:
                img = Image.open(img_path)
                imgs_1.append(self.transform(img))
                imgs_2.append(self.transform(img))
                img.close()

            imgs_1 = torch.stack(imgs_1, dim=0)
            imgs_2 = torch.stack(imgs_2, dim=0)
            labels = torch.Tensor(labels).long()
            domains = torch.Tensor(domains).long()

            yield imgs_1, imgs_2, labels, domains
