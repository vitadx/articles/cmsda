# Config for data
JSON_DATASET_PATH = "./../data/mini_domain_net.json"
DATA_ROOT_DIR = "./../data/domain_net/"

# Config for CMSDA
MODEL_NAME = "resnet18"
PROJ_HEAD_DIM = 256

# Config for training
SOURCE_BATCH_SIZE = 256
TARGET_SOURCE_BATCH_SIZE_RATIO = 1
TARGET_BATCH_SIZE = int(TARGET_SOURCE_BATCH_SIZE_RATIO * SOURCE_BATCH_SIZE)
STEPS_PER_EPOCH = 100
EPOCHS = 600
BASE_LR = 1e-4
SAMPLED_EXAMPLES_PER_CLASS_PER_DOMAIN = 4
USE_MIXED_PRECISION = True
TEMPERATURE = 0.1
PROB_THRESHOLD_FIXMATCH = 0.95
LAMBDA_ISCL = TEMPERATURE
LAMBDA_UNSUP = 2
MIXUP_ALPHA = 0.4
EMA_DECAY = 0.999
RESIZED_SHAPE = (96, 96)
NUM_WORKERS = 10

# Config for logs, checkpoints and results directories
LOG_DIR = "./../runs/mini_domain_net/test/"
CHECKPOINT_DIR = "./../checkpoints/mini_domain_net/test/"
RESULTS_DIR = "./../results/mini_domain_net/test/"
