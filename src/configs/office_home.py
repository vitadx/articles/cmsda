# Config for data
JSON_DATASET_PATH = "./../data/office_home.json"
DATA_ROOT_DIR = "./../data/office_home/"

# Config for CMSDA
MODEL_NAME = "resnet50"
PROJ_HEAD_DIM = 256

# Config for training
SOURCE_BATCH_SIZE = 128
TARGET_SOURCE_BATCH_SIZE_RATIO = 1
TARGET_BATCH_SIZE = int(TARGET_SOURCE_BATCH_SIZE_RATIO * SOURCE_BATCH_SIZE)
STEPS_PER_EPOCH = 100
EPOCHS = 200
BASE_LR = 1e-4
SAMPLED_EXAMPLES_PER_CLASS_PER_DOMAIN = 4
USE_MIXED_PRECISION = True
TEMPERATURE = 0.1
PROB_THRESHOLD_FIXMATCH = 0.95
LAMBDA_ISCL = TEMPERATURE
LAMBDA_UNSUP = 1
MIXUP_ALPHA = 0.2
EMA_DECAY = 0.999
RESIZED_SHAPE = (224, 224)
NUM_WORKERS = 10

# Config for logs, checkpoints and results directories
LOG_DIR = "./../runs/office_home/test/"
CHECKPOINT_DIR = "./../checkpoints/office_home/test/"
RESULTS_DIR = "./../results/office_home/test/"
