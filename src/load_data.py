import os
import numpy as np
import pandas as pd


def split_json_dataset(json_dataset, data_root_dir, source_target_domains):
    # Check that specified source and target domains are indeed in the
    # domain net dataset
    for domain_category in source_target_domains:
        for domain in source_target_domains[domain_category]:
            assert domain in json_dataset["domain2index"], "{} is not a valid domain name".format(
                domain)

    splitted_data = {}
    for domain_category in source_target_domains:
        if domain_category not in splitted_data:
            splitted_data[domain_category] = {}
        for domain in source_target_domains[domain_category]:
            for split in json_dataset["data"][domain]:
                if split not in splitted_data[domain_category]:
                    splitted_data[domain_category][split] = {
                        "img_paths": [],
                        "labels": [],
                        "domains": [],
                    }
                for example in json_dataset["data"][domain][split]:
                    splitted_data[domain_category][split]["img_paths"].append(
                        os.path.join(data_root_dir, example["img_path"]))
                    splitted_data[domain_category][split]["labels"].append(
                        json_dataset["label2index"][example["label"]])
                    splitted_data[domain_category][split]["domains"].append(
                        json_dataset["domain2index"][domain])
    return splitted_data


def display_datasets_stats(datasets, index2label, index2domain):
    for domain_category in datasets:
        for split in datasets[domain_category]:
            dataset = datasets[domain_category][split]
            labels = np.array([index2label[str(label)]
                               for label in dataset.labels])
            domains = np.array([index2domain[str(domain)]
                                for domain in dataset.domains])
            print("{} / {} :".format(domain_category, split))
            data = {"domains": [], "num_classes": [], "num_examples": []}
            for domain in np.unique(domains):
                data["domains"].append(domain)
                data["num_classes"].append(
                    np.unique(labels[domains == domain]).size)
                data["num_examples"].append(len(labels[domains == domain]))
            print(pd.DataFrame(data).to_markdown())
            print()
