from torch import nn
from torchvision import models as pretained_models

pretrained_models_func = {
    model_name: getattr(pretained_models, model_name) for model_name
    in dir(pretained_models)}


class FeaturesExtractor(nn.Module):

    def __init__(
            self,
            model_name,
            norm_layer,
            pretrained):
        super(FeaturesExtractor, self).__init__()
        assert model_name in pretrained_models_func, "{} is not a valid pretrained model".format(
            model_name)
        model = pretrained_models_func[model_name](
            pretrained=pretrained,
            norm_layer=norm_layer)
        self.model = model
        self.out_features = self.model.fc.in_features
        self.model.fc = nn.Identity()

    def forward(self, x):
        x = self.model(x)
        return x


class ProjectionHead(nn.Module):

    def __init__(self, input_dim, output_dim):
        super(ProjectionHead, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.fc1 = nn.Linear(in_features=input_dim, out_features=input_dim)
        self.bn1 = nn.BatchNorm1d(num_features=input_dim)
        self.fc2 = nn.Linear(in_features=input_dim, out_features=output_dim)

    def forward(self, x):
        x = nn.LeakyReLU()(self.bn1(self.fc1(x)))
        z = self.fc2(x)
        return z


class ClassificationHead(nn.Module):

    def __init__(self, input_dim, num_classes):
        super(ClassificationHead, self).__init__()
        self.input_dim = input_dim
        self.num_classes = num_classes
        self.head = nn.Linear(input_dim, num_classes)

    def forward(self, x):
        y = self.head(x)
        return y


class CMSDA(nn.Module):

    def __init__(
            self,
            model_name,
            norm_layer,
            pretrained,
            proj_head_dim,
            num_classes):
        super(CMSDA, self).__init__()
        self.backbone = FeaturesExtractor(
            model_name=model_name,
            norm_layer=norm_layer,
            pretrained=pretrained,)
        self.projection_head = ProjectionHead(
            input_dim=self.backbone.out_features,
            output_dim=proj_head_dim,)
        self.classification_head = ClassificationHead(
            input_dim=self.backbone.out_features,
            num_classes=num_classes,)

    def forward(self, x, compute_z=True):
        h = self.backbone(x)
        y = self.classification_head(h)
        if compute_z:
            z = self.projection_head(h)
        else:
            z = None
        return z, y
