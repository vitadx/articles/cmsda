import os
import json
import pprint
import numpy as np

import torch
from torch import nn
from torch.optim import Adam
from torch.optim.lr_scheduler import LambdaLR
from torch.utils.data import DataLoader
from torchvision import transforms

from configs import domain_net as config
import load_data
from datasets import (
    ClassificationDataset,
    TwoStreamsDataset,
    BalancedClassDomainTwoStreamDataset)
from augmentations import RandAugment
from dataloaders import InfiniteDataLoader
from models import CMSDA
from ema import EMA
from trainers import Trainer
from evaluators import Evaluator


def main():

    # Get hyperparameters
    hyperparameters = {attr: getattr(config, attr) for attr in dir(
        config) if "__" not in attr}
    pprint.pprint(hyperparameters)
    DEVICE = torch.device("cuda")

    # Read json containing all DomainNet dataset info
    with open(config.JSON_DATASET_PATH, "r") as f:
        json_dataset = json.load(f)
    domains = list(json_dataset["domain2index"].keys())

    # Create transformations
    imagenet_normalization = transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225])
    weak_transform = transforms.Compose([
        transforms.Resize(size=config.RESIZED_SHAPE),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.ToTensor(),
        imagenet_normalization,
    ])
    strong_transform = transforms.Compose([
        transforms.Resize(size=config.RESIZED_SHAPE),
        transforms.RandomHorizontalFlip(p=0.5),
        RandAugment(n=2, p=1),
        transforms.RandomResizedCrop(
            size=config.RESIZED_SHAPE, scale=(0.8, 1)),
        transforms.ToTensor(),
        imagenet_normalization,
    ])
    test_transform = transforms.Compose([
        transforms.Resize(size=config.RESIZED_SHAPE),
        transforms.ToTensor(),
        imagenet_normalization,
    ])

    for exp_index in range(1):
        for domain_index, domain in enumerate(domains):
            source_domains = list(domains)
            target_domain = source_domains.pop(domain_index)

            source_target_domains = {
                "source": source_domains,
                "target": [target_domain]}

            # Create log, checkpoints and results directory paths
            log_dir = os.path.join(
                config.LOG_DIR, target_domain, "exp_{}".format(exp_index+1))
            checkpoint_dir = os.path.join(
                config.CHECKPOINT_DIR, target_domain, "exp_{}".format(exp_index+1))
            results_dir = os.path.join(
                config.RESULTS_DIR, target_domain, "exp_{}".format(exp_index+1))

            # Split the data according source/target train/test
            splitted_data = load_data.split_json_dataset(
                json_dataset=json_dataset,
                data_root_dir=config.DATA_ROOT_DIR,
                source_target_domains=source_target_domains)

            # Get the datasets
            datasets = {"source": {}, "target": {}}
            datasets["source"]["train"] = BalancedClassDomainTwoStreamDataset(
                **splitted_data["source"]["train"],
                batch_size=config.SOURCE_BATCH_SIZE,
                sampled_examples_per_class_per_domain=config.SAMPLED_EXAMPLES_PER_CLASS_PER_DOMAIN,
                transform=strong_transform,
                index2label=json_dataset["index2label"],
                index2domain=json_dataset["index2domain"],
                shuffle=True)
            datasets["target"]["train"] = TwoStreamsDataset(
                **splitted_data["target"]["train"],
                weak_transform=weak_transform,
                strong_transform=strong_transform,
                index2label=json_dataset["index2label"],
                index2domain=json_dataset["index2domain"])

            for domain_category in ["source", "target"]:
                datasets[domain_category]["test"] = ClassificationDataset(
                    **splitted_data[domain_category]["test"],
                    transform=test_transform,
                    index2label=json_dataset["index2label"],
                    index2domain=json_dataset["index2domain"])

            # Display datasets statistics
            load_data.display_datasets_stats(
                datasets=datasets,
                index2label=json_dataset["index2label"],
                index2domain=json_dataset["index2domain"])

            # Get the dataloaders
            dataloaders = {"source": {}, "target": {}}
            dataloaders["source"]["train"] = DataLoader(
                dataset=datasets["source"]["train"],
                num_workers=config.NUM_WORKERS,
                batch_size=1,
                shuffle=False,
                drop_last=True)
            dataloaders["target"]["train"] = InfiniteDataLoader(
                dataset=datasets["target"]["train"],
                num_workers=config.NUM_WORKERS,
                batch_size=config.TARGET_BATCH_SIZE,
                shuffle=True,
                drop_last=True)
            dataloaders["source"]["test"] = DataLoader(
                dataset=datasets["source"]["test"],
                num_workers=config.NUM_WORKERS,
                batch_size=config.SOURCE_BATCH_SIZE,
                shuffle=True,
                drop_last=False)
            dataloaders["target"]["test"] = DataLoader(
                dataset=datasets["target"]["test"],
                num_workers=config.NUM_WORKERS,
                batch_size=config.SOURCE_BATCH_SIZE,
                shuffle=True,
                drop_last=False)

            # Create CMSDA
            cmsda = CMSDA(
                model_name=config.MODEL_NAME,
                norm_layer=nn.BatchNorm2d,
                pretrained=True,
                proj_head_dim=config.PROJ_HEAD_DIM,
                num_classes=len(json_dataset["label2index"]))
            cmsda_ema = EMA(cmsda, decay=config.EMA_DECAY)
            cmsda = nn.DataParallel(cmsda)
            cmsda_ema = nn.DataParallel(cmsda_ema).eval()

            # Create optimizer and scheduler
            optimizer = Adam(
                params=cmsda.parameters(),
                lr=config.BASE_LR)
            # Cosine decay learning rate scheduler
            scheduler = LambdaLR(
                optimizer,
                lr_lambda=lambda epoch: 0.5 *
                (1 + np.cos(np.pi * epoch / config.EPOCHS)),
                verbose=False)

            # Create log dir if it doesn't exist
            os.makedirs(log_dir, exist_ok=True)

            # Pretrain the model only on source
            trainer = Trainer()
            trainer.train(
                cmsda=cmsda,
                cmsda_ema=cmsda_ema,
                dataloaders=dataloaders,
                optimizer=optimizer,
                scheduler=scheduler,
                epochs=config.EPOCHS,
                temperature=config.TEMPERATURE,
                prob_threshold_fixmatch=config.PROB_THRESHOLD_FIXMATCH,
                lambda_unsup=config.LAMBDA_UNSUP,
                lambda_iscl=config.LAMBDA_ISCL,
                mixup_alpha=config.MIXUP_ALPHA,
                device=DEVICE,
                use_mixed_precision=config.USE_MIXED_PRECISION,
                steps_per_epoch=config.STEPS_PER_EPOCH,
                log_dir=log_dir)

            # Evaluate on the source test dataset, target test dataset
            evaluator = Evaluator()
            print("Source Test:")
            source_perfs_per_domains = evaluator.evaluate(
                cmsda=cmsda_ema,
                dataloader=dataloaders["source"]["test"],
                use_mixed_precision=config.USE_MIXED_PRECISION,
                device=DEVICE,
                display_results=True)
            print("Target Test:")
            target_perfs_per_domains = evaluator.evaluate(
                cmsda=cmsda_ema,
                dataloader=dataloaders["target"]["test"],
                use_mixed_precision=config.USE_MIXED_PRECISION,
                device=DEVICE,
                display_results=True)

            # Dump model
            os.makedirs(checkpoint_dir, exist_ok=True)
            model_path = os.path.join(
                checkpoint_dir, config.MODEL_NAME+".pth")
            torch.save(
                {"cmsda": cmsda_ema.module.state_dict(),
                 "hyperparameters": hyperparameters},
                model_path)

            # Dump results as json
            os.makedirs(results_dir, exist_ok=True)
            results_path = os.path.join(
                results_dir, config.MODEL_NAME+".json")
            results = {
                "hyperparameters": hyperparameters,
                "source": source_perfs_per_domains,
                "target": target_perfs_per_domains,
            }

            with open(results_path, "w") as f:
                json.dump(results, f, indent=4)

            break


if __name__ == "__main__":
    main()
