import tqdm
import pandas as pd
import numpy as np

import torch


class Evaluator():

    def __init__(self):
        pass

    def evaluate(
            self,
            cmsda,
            dataloader,
            use_mixed_precision,
            device,
            display_results=False):

        index2domain = dataloader.dataset.index2domain

        # Set mode to eval
        cmsda = cmsda.eval()

        # Move moddel to appropriate device
        cmsda = cmsda.to(device)

        target = []
        preds = []
        domains = []

        with torch.no_grad():
            for batch_index, (X, labs, doms) in enumerate(tqdm.tqdm(dataloader)):
                with torch.cuda.amp.autocast(enabled=use_mixed_precision):
                    X = X.to(device)
                    _, pred = cmsda(X, compute_z=False)
                    pred = pred.argmax(dim=-1)

                    preds.append(pred.cpu().detach().numpy())
                    target.append(labs.numpy())
                    domains.append(doms.numpy())

        preds = np.concatenate(preds)
        target = np.concatenate(target)
        domains = np.concatenate(domains)

        perfs_per_domains = {"domain": [], "accuracy": []}
        for domain in np.unique(domains):
            bool_vec = domains == domain
            domain_index = domain
            domain_label = index2domain[str(domain_index)]

            acc = (preds[bool_vec] == target[bool_vec]).sum() / bool_vec.sum()
            perfs_per_domains["domain"].append(domain_label)
            perfs_per_domains["accuracy"].append(acc.item())

        global_acc = (preds == target).sum() / target.size
        perfs_per_domains["domain"].append("all")
        perfs_per_domains["accuracy"].append(global_acc.item())
        if display_results:
            print(pd.DataFrame(data=perfs_per_domains).to_markdown())
        return {
            dom: acc for dom, acc in zip(
                perfs_per_domains["domain"], perfs_per_domains["accuracy"])}
